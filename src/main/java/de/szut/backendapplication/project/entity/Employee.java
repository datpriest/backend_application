package de.szut.backendapplication.project.entity;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Collection;
import java.util.List;

@Entity
@Getter
@Setter
public class Employee {
    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER,
                cascade = CascadeType.ALL)
    @JoinColumn(name="project", nullable=false)
    private Project project;

    @ManyToMany(fetch = FetchType.EAGER)
    private List<Qualification> qualification;

}
