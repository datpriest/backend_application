package de.szut.backendapplication.project.entity;

import de.szut.backendapplication.project.Status;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "project")
public class Project {
//TODO: Spring Boot Validierungen hinzufügen für die Attribute
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String name;
    private String description;
    private Status status;
    private String comment;

    private Date startDate;
    private Date plannedEndDate;
    private Date finalEndDate;

    private Long customerId;
    private String customerName;
    private String customerContactName;

    private Long projectLeadId;

    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(mappedBy = "project", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    public List<Employee> employeeList;

    public void addEmployee(Employee employee) { employeeList.add(employee);}

    public String toString() {
        String string = "";
        string += "\nName: " + this.getName();
        string += "\nstartDate" + this.getStartDate();
        string += "\nStatus" + this.getStatus();
        string += "\nstartDate" + this.getStartDate();
        return string;
    }


}

