package de.szut.backendapplication.project;

import de.szut.backendapplication.exceptionHandling.ResourceNotFoundException;
import de.szut.backendapplication.project.dto.*;
import de.szut.backendapplication.project.entity.Employee;
import de.szut.backendapplication.project.entity.Project;
import org.springframework.stereotype.Service;

import javax.validation.Valid;
import java.time.Instant;
import java.util.*;
import java.util.logging.Logger;

@Service
public class ProjectMapper {
    final Logger logger;

    public ProjectMapper() {
        this.logger = Logger.getGlobal();
    }

    public GetProjectDto mapToGetDto(Project entity) {
        if (entity == null) {
            throw new ResourceNotFoundException("Resource not found!");
        }
        return new GetProjectDto(
                entity.getId(),
                entity.getName(),
                entity.getDescription(),
                entity.getCustomerId(),
                entity.getCustomerName(),
                entity.getCustomerContactName(),
                entity.getStatus(),
                entity.getEmployeeList()
        );
    }

    public GetAllActiveProjectsDto mapToGetAllActiveProjectsDto(Set<Project> projectList) {
        Set<GetProjectDto> dtoList = new java.util.HashSet<>(Set.of());
        for(Project project : projectList) {
            if (project.getStatus() == Status.ACTIVE) {
                var dto = mapToGetDto(project);
                logger.info(dto.toString());
                dtoList.add(mapToGetDto(project));
            }
        }

        return new GetAllActiveProjectsDto(dtoList);
    }

    public Project mapCreateDtoToEntity(@Valid CreateProjectDto dto) {
        Project entity = new Project();

        entity.setName(dto.getName());
        entity.setDescription(dto.getDescription());
        entity.setComment(dto.getComment());
        if (entity.getStartDate() == null) entity.setStartDate(Date.from(Instant.now()));
        else entity.setStartDate(dto.getStartDate());
        entity.setPlannedEndDate(dto.getPlannedEndDate());
        entity.setCustomerId(dto.getCustomerId());
        entity.setCustomerName(dto.getCustomerName());
        entity.setCustomerContactName(dto.getCustomerContactName());
        entity.setProjectLeadId(dto.getProjectLeadId());
        if (dto.getStatus() == null) entity.setStatus(Status.ACTIVE);
        else entity.setStatus(dto.getStatus());

        return entity;
    }

    public GetAllProjectsByEmployeeIdDto mapProjectsAndEmployeeIdToGetAllProjectsByEmployeeIdDto(long id, List<GetProjectDto> projects) {
        GetAllProjectsByEmployeeIdDto dto = new GetAllProjectsByEmployeeIdDto();
        dto.setEmployeeId(id);
        Set<Long> allProjects = new HashSet<>();
        for (GetProjectDto projectDto: projects) {
            allProjects.add(projectDto.getId());
        }
        dto.setAllProjects(allProjects);
        return dto;
    }

    public GetAllEmployeesByProjectIdDto mapEmployeesAndProjectIdToGetAllEmployeesByProjectIdDto(Project entity) {
        GetAllEmployeesByProjectIdDto dto = new GetAllEmployeesByProjectIdDto();
        dto.setProjectId(entity.getId());
        dto.setName(entity.getName());
        dto.setProjectLeadId(entity.getProjectLeadId());
        Set<EmployeeIdDto> allEmployees = new HashSet<>();
        for (Employee employee: entity.getEmployeeList()) {
            allEmployees.add(mapEmployeeToEmployeeIdDto(employee));
        }
        dto.setAllEmployees(allEmployees);
        return dto;
    }

    public EmployeeIdDto mapEmployeeToEmployeeIdDto(Employee entity) {
        EmployeeIdDto dto = new EmployeeIdDto();
        dto.setId(entity.getId());
        return dto;
    }

    public AddEmployeeWithQualificationToProjectDto mapEmployeeWithQualificationToProjectDto(Employee employee){
        AddEmployeeWithQualificationToProjectDto dto = new AddEmployeeWithQualificationToProjectDto();
        dto.setEmployeeid(employee.getId());
        dto.setDesignation(dto.getDesignation());
        return dto;
    }

    public Project mapToUpdateProjectDto(UpdateProjectDto dto, Project entity) {
        entity.setId(dto.getId());
        if (dto.getName().isPresent())
            entity.setName(dto.getName().get());
        if (dto.getDescription().isPresent())
            entity.setDescription(dto.getDescription().get());
        if (dto.getComment().isPresent())
            entity.setComment(dto.getComment().get());
        if (dto.getStartDate().isPresent())
            entity.setStartDate(dto.getStartDate().get());
        if (dto.getPlannedEndDate().isPresent())
            entity.setPlannedEndDate(dto.getPlannedEndDate().get());
        if (dto.getFinalEndDate().isPresent())
            entity.setFinalEndDate(dto.getFinalEndDate().get());
        if (dto.getCustomerId().isPresent())
            entity.setCustomerId(dto.getCustomerId().get());
        if (dto.getCustomerName().isPresent())
            entity.setCustomerName(dto.getCustomerName().get());
        if (dto.getCustomerContactName().isPresent())
            entity.setCustomerContactName(dto.getCustomerContactName().get());
        if (dto.getProjectLeadId().isPresent())
            entity.setProjectLeadId(dto.getProjectLeadId().get());
        if (dto.getStatus().isPresent()) {
            entity.setStatus(dto.getStatus().get());
        }

        return entity;
    }
}
