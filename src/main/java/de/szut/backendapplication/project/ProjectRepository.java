package de.szut.backendapplication.project;

import de.szut.backendapplication.project.entity.Employee;
import de.szut.backendapplication.project.entity.Project;
import de.szut.backendapplication.project.entity.Qualification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProjectRepository extends JpaRepository<Project, Long> {
    List<Project> findByName(String name);
}
