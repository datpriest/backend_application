package de.szut.backendapplication.project;

import de.szut.backendapplication.exceptionHandling.ResourceNotFoundException;
import de.szut.backendapplication.exceptionHandling.TooLessDetailsException;
import de.szut.backendapplication.project.dto.*;
import de.szut.backendapplication.project.entity.Employee;
import de.szut.backendapplication.project.entity.Project;
import de.szut.backendapplication.project.entity.Qualification;
import de.szut.backendapplication.project.service.EmployeeService;
import de.szut.backendapplication.project.service.ProjectService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/api/v1/project")
public class ProjectController {
    private final ProjectService service;
    private final EmployeeService employeeService;
    private final ProjectMapper mapper;
    final Logger logger;

    public ProjectController(ProjectService service, ProjectMapper mappingService, EmployeeService _employeeService) {
        this.service = service;
        this.mapper = mappingService;
        this.employeeService = _employeeService;
        this.logger = Logger.getGlobal();
    }

    @Operation(summary = "creates a new Project with its name and description")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "created project",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = GetProjectDto.class))}),
            @ApiResponse(responseCode = "400", description = "invalid JSON posted",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "not authorized",
                    content = @Content)})
    @PostMapping
    public ResponseEntity<GetProjectDto> create(@RequestBody @Valid CreateProjectDto projectCreateDto) {
        Project project = this.mapper.mapCreateDtoToEntity(projectCreateDto);
        project = this.service.create(project);
        return new ResponseEntity<>(this.mapper.mapToGetDto(project), HttpStatus.CREATED);
    }

    @Operation(summary = "Getting a Project with its name and description")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Getting a Project by its name",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = GetProjectDto.class))}),
            @ApiResponse(responseCode = "400", description = "invalid JSON posted",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "not found",
                    content = @Content)})
    @GetMapping("/byName/{name}")
    public ResponseEntity<List<GetProjectDto>> getByName(@PathVariable String name) {
        List<Project> projectList = this.service.findByName(name);
        ArrayList<GetProjectDto> projectGetDtoList = new ArrayList<>();
             for (Project project : projectList) {
                 if (project != null)
                    projectGetDtoList.add(this.mapper.mapToGetDto(project));
                 else
                     Logger.getGlobal().info(project.toString());
             }
        return new ResponseEntity<>(projectGetDtoList, HttpStatus.OK);
    }

    @Operation(summary = "Gets the Project with the given id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "get the project by id",
                    content = {@Content(mediaType = "application/json",
                        schema = @Schema(implementation = GetProjectDto.class))}),
            @ApiResponse(responseCode = "400", description = "invalid JSON posted",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "not found",
                    content = @Content)})
    @GetMapping("/id/{id}")
    public ResponseEntity<GetProjectDto> getProjectById(@PathVariable final Long id) {
        GetProjectDto project = this.mapper.mapToGetDto(this.service.readById(id));
        if (project == null) {
            throw new ResourceNotFoundException(String.format("project not found on id : %s", id));
        }

        return new ResponseEntity<>(project, HttpStatus.OK);
    }

    @Operation(summary = "Gets all project by EmployeeId")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "get all projects from the employee",
                    content = {@Content(mediaType = "application/json",
                        schema = @Schema(implementation = GetAllProjectsByEmployeeIdDto.class))}),
            @ApiResponse(responseCode = "400", description = "invalid JSON posted",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "not found",
                    content = @Content)})
    @GetMapping("/byEmployee/{id}/")
    public ResponseEntity<GetAllProjectsByEmployeeIdDto> getAllProjectsByEmployeeId(@PathVariable final Long id) {
        List<Project> projects = this.service.readAll();
        List<GetProjectDto> list = new LinkedList<>();
        for (Project project: projects) {
            if (project.getEmployeeList().contains(id)) {
                list.add(this.mapper.mapToGetDto(project));
            }
        }
        GetAllProjectsByEmployeeIdDto dto = this.mapper.mapProjectsAndEmployeeIdToGetAllProjectsByEmployeeIdDto(id, list);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @Operation(summary = "Start a Request to get all employees to the ProjectId")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Get all employees to the Project id",
                    content = {@Content(mediaType = "application/json",
                        schema = @Schema(implementation = GetAllEmployeesByProjectIdDto.class))}),
            @ApiResponse(responseCode = "400", description = "invalid JSON posted",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "not found",
                    content = @Content)})
    @GetMapping("/{id}/allEmployees")
    public ResponseEntity<GetAllEmployeesByProjectIdDto> getAllEmployeesByProjectId(@PathVariable final Long id) {
        Project searchedProject = this.service.readById(id);
        if (searchedProject == null) {
            throw new ResourceNotFoundException("Project not found on id:" + id);
        } else {
            GetAllEmployeesByProjectIdDto dto = this.mapper.mapEmployeesAndProjectIdToGetAllEmployeesByProjectIdDto(searchedProject);
            return new ResponseEntity<>(dto, HttpStatus.OK);
        }
    }

    @Operation(summary = "Start a Request to delete a project by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Delete was complete",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "not found",
                    content = @Content)})
    @DeleteMapping("/{id}")
    public ResponseEntity<HttpStatus> deleteProject(@PathVariable final Long id) {
      Project project = this.service.readById(id);
        if (project == null) {
            throw new ResourceNotFoundException("project not found on id = " + id);
        } else {
            this.service.delete(project);
            return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
        }
    }

    @Operation(summary = "Start a Request to get all projects")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Get all projects",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = GetAllEmployeesByProjectIdDto.class))}),
            @ApiResponse(responseCode = "400", description = "invalid JSON posted",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "not found",
                    content = @Content)})
    @GetMapping
    public ResponseEntity<GetAllActiveProjectsDto> getAllProjects() {
        List<Project> projectList = this.service.readAll();
        ArrayList<GetProjectDto> dtoList = new ArrayList<>(projectList.size());

        Set<Project> newDtoList = new LinkedHashSet<>();

        for (Project project : projectList) {
            dtoList.add(this.mapper.mapToGetDto(project));
            newDtoList.add(project);

        }
        return new ResponseEntity<>(this.mapper.mapToGetAllActiveProjectsDto((Set<Project>) projectList), HttpStatus.OK);

    }

    @Operation(summary = "updates the project")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "updated project",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = UpdateProjectDto.class))}),
            @ApiResponse(responseCode = "400", description = "invalid JSON posted",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "not authorized",
                    content = @Content)})
    @PutMapping
    public ResponseEntity<UpdateObject> UpdateProjectById(@RequestBody @Valid UpdateProjectDto dto) {
        if (dto.getId() == null) throw new TooLessDetailsException("UpdateParameter are invalid!");
        Project project = this.service.readById(dto.getId());
        GetProjectDto getDto = this.mapper.mapToGetDto(project);
        if (project == null){
            this.logger.warning("Project is null!" + dto.toString());
            throw new ResourceNotFoundException(String.format("Project with id: %s", dto.getId()));
        }
        Project newProject = mapper.mapToUpdateProjectDto(dto, project);
        this.service.update(newProject);
        return new ResponseEntity<>(new UpdateObject(getDto, this.mapper.mapToGetDto(newProject)), HttpStatus.OK);
    }

    @PutMapping("/addEmployeeWithQualificationToProject/{projectId}/{qualificationDesignation}/{employeeId}")
    public ResponseEntity<AddEmployeeWithQualificationToProjectDto>
        AddEmployeeWithQualificationToProject( @PathVariable Long projectId,
                                               String qualificationDesignation,
                                               Long employeeId ){
        /*
        Qualification qualification = GetQualificationByDesignation(qualificationDesignation);
        Employee employee = employeeService.getById(employeeId);
        if(qualification== null) {
            throw new ResourceNotFoundException("qualification doesn't exist");
        }
        else if (employee.getQualification().contains(qualification)) {
           AddEmployeeWithQualificationToProjectDto dto = this.mapper.mapEmployeeWithQualificationToProjectDto(employee);

           return new ResponseEntity<>(dto, HttpStatus.OK);
        }

        throw new ResourceNotFoundException("qualification not found");*/
        throw new ResourceNotFoundException("Function does not working fine, greetings Lucas, Leon");
    }

}


/// Struct to hold Data
@Data
@Setter
@Getter
@AllArgsConstructor
class UpdateObject {
    private GetProjectDto oldDto;
    private GetProjectDto newDto;
}