package de.szut.backendapplication.project.service;

import de.szut.backendapplication.project.ProjectRepository;
import de.szut.backendapplication.project.entity.Employee;
import de.szut.backendapplication.project.entity.Project;
import de.szut.backendapplication.project.entity.Qualification;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

@Service
public class ProjectService {
    private final ProjectRepository repository;
    private final Logger logger;

    public ProjectService(ProjectRepository repository) {
        this.repository = repository;
        this.logger = Logger.getGlobal();
    }

    public Project create(Project entity) {
        Logger.getGlobal().info(entity.toString());
        return this.repository.save(entity);
    }

    public List<Project> readAll() {
        return this.repository.findAll();
    }

    public Project readById(long id) {
        Optional<Project> project = this.repository.findById(id);
        if (project.isEmpty()) {
            return null;
        }
        return project.get();
    }


    public void delete(Project entity) {
        this.repository.delete(entity);
    }

    public List<Project> findByName(String name) {
        return this.repository.findByName(name);
    }

    public void update(Project entity) {
        this.repository.save(entity);
    }
}
