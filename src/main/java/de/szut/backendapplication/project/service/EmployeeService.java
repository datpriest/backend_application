package de.szut.backendapplication.project.service;

import de.szut.backendapplication.project.dto.EmployeeIdDto;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class EmployeeService {

    private RestTemplate restTemplate;
    private final String url = "https://employee.szut.dev";

    public EmployeeService(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }

    public EmployeeIdDto getById(Long id) {
        return this.restTemplate.getForObject(url +"/employees/{id}", EmployeeIdDto.class, id);
    }
}
