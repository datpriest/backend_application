package de.szut.backendapplication.project;

public enum Status {
    ACTIVE,
    COMPLETED,
    ARCHIVED,
    CANCELED,
    DELETED,
}
