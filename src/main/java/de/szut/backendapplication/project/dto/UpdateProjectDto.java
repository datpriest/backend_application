package de.szut.backendapplication.project.dto;

import de.szut.backendapplication.project.Status;
import de.szut.backendapplication.project.entity.Employee;
import de.szut.backendapplication.project.entity.Qualification;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@Getter
@Setter
@Data
public class UpdateProjectDto {
    @NotBlank
    private Long id;

    private Optional<String> name;

    private Optional<String> description;
    private Optional<String> comment;

    private Optional<Date> startDate;
    private Optional<Date> plannedEndDate;
    private Optional<Date> finalEndDate;

    private Optional<Long> customerId;
    private Optional<String> customerName;
    private Optional<String> customerContactName;

    private Optional<Status> status;

    private Optional<List<Employee>> employeeList;

    private Optional<Long> projectLeadId;
}
