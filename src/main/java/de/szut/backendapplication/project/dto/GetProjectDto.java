package de.szut.backendapplication.project.dto;

import de.szut.backendapplication.project.Status;
import de.szut.backendapplication.project.entity.Employee;
import de.szut.backendapplication.project.entity.Qualification;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@AllArgsConstructor
@Getter
@Setter
@Data
public class GetProjectDto {
    private Long id;

    private String name;

    private String description;

    private Long customerId;
    private String customerName;
    private String customerContactName;
    private Status status;

    private List<Employee> employeeList;


}
