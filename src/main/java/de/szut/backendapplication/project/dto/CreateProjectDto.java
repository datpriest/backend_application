package de.szut.backendapplication.project.dto;

import de.szut.backendapplication.project.Status;
import de.szut.backendapplication.project.entity.Qualification;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;

@Data
public class CreateProjectDto {
    @NotBlank(message = "Name is mandatory")
    @Size(max = 50, message = "Name must not exceed 50 characters")
    private String name;

    @Size(max = 300, message = "Description must not exceed 300 characters")
    private String description;

    @NotBlank(message = "Customer ID is mandatory!")
    private Long customerId;

    @NotBlank(message = "Customer Name is mandatory!")
    private String customerName;

    @NotBlank(message = "Customer Contact Name is mandatory")
    private String customerContactName;

    private Status status;

    @Size(max = 300, message = "Comment must not exceed 300 characters")
    private String comment;

    @NotBlank(message = "Project Lead is mandatory")
    private Long projectLeadId;

    @NotBlank(message = "Start date is mandatory")
    private Date startDate;

    @NotBlank(message = "Planned end date is mandatory")
    private Date plannedEndDate;

    private List<Qualification> qualificationList;
}
