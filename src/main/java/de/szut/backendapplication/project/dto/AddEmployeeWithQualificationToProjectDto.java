package de.szut.backendapplication.project.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
@Data
public class AddEmployeeWithQualificationToProjectDto {
    Long employeeid;
    String designation;

    public AddEmployeeWithQualificationToProjectDto() {

    }
}
