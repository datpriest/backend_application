package de.szut.backendapplication.project.dto;

import de.szut.backendapplication.project.Status;
import de.szut.backendapplication.project.entity.Employee;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.OneToMany;
import java.util.Date;
import java.util.List;
import java.util.Set;

@AllArgsConstructor
@Getter
@Setter
public class GetProjectByIdDto {
    private long id;
    private String name;
    private String description;
    private Status status;
    private String comment;
    private Date startDate;
    private Date plannedEndDate;
    private Date finalEndDate;
    private Long customerId;
    private String customerName;
    private String customerContactName;
    private List<Employee> employeeList;
    private Long projectLeadId;
}