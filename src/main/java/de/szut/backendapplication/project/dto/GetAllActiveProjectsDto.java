package de.szut.backendapplication.project.dto;

import lombok.Data;

import java.util.Set;

@Data
public class GetAllActiveProjectsDto {
    private final Set<GetProjectDto> activeProjects;

    public GetAllActiveProjectsDto(Set<GetProjectDto> dtoList) {
        this.activeProjects = dtoList;
    }
}
