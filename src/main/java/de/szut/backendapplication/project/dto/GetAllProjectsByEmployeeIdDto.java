package de.szut.backendapplication.project.dto;

import lombok.Data;

import java.util.Set;

@Data
public class GetAllProjectsByEmployeeIdDto {
    private long employeeId;
    private Set<Long> allProjects;
}
