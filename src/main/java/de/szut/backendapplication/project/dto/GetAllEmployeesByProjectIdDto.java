package de.szut.backendapplication.project.dto;

import lombok.Data;

import java.util.Set;

@Data
public class GetAllEmployeesByProjectIdDto {
    private long projectId;
    private String name;
    private long projectLeadId;
    private Set<EmployeeIdDto> allEmployees;
}
