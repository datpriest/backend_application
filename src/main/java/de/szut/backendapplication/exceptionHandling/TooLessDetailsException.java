package de.szut.backendapplication.exceptionHandling;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class TooLessDetailsException extends RuntimeException {
    public TooLessDetailsException(String message) {
        super(message);
    }
}