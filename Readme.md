
## Endpunkt
```
http://localhost:8089
BaseRoute : "api/v1
BaseRoute für Project = "api/v1/project
BaseRoute hat Put, Get, Post, Delete auf der selben Route mit den jeweiligen Methoden
POST required bodyParameter (name, customerId) return posted Project StatusCode 201
GET required bodyParameter (none) return all Projects StatusCode 200
PUT required bodyParameter (id) return old project and new project StatusCode 200
DELETE required pathParameter (id) return no Content Statuscode 204

GET /{id}/allEmployees required pathParameter (id) return allEmployees to Project
GET /byName/{name} required pathParameter (name) return all Projects with Name (name)
GET /id/{id} required pathParameter (id) return Project with id (id)

PUT /addEmployeeWithQualificationToProject/{projectId}/{qualificationDesignation}/{employeeId} 
        required PathParameter (projectId, qualificationDesignation, employeeId) not finished !!!!!



Alle dokumentierten Pfade funktionieren makellos., bisauf Letzterer.



```
## Swagger
```
http://localhost:8089/swagger
```